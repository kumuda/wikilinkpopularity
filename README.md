**Summary:**

WikiLinkPopularity ia a console application, that takes wikipedia link as input, and the result will be the most popular wikipedia link within the given input page.  
Along with the input url, 2 other inputs are required. They are Level of depth and No of links to search.    
* An integer which decides the level of depth to go in search of links to decide the link popularity and   
* Number of links that should be considered in each page at each level.   

**Steps to run the App:  
**
  
To compile the project and then run:   
Step 1. Make sure you are in WikiLinkPopularity folder, and type the below command:    
       javac -d target -classpath lib/jsoup-1.8.3.jar:lib/xsoup-0.1.0.jar  src/*.java      

Step 2. Change the directory to target folder present directly under WikiLinkPopularity an type the below command:   
       java -Xms300m -Xmx3092m -cp ../lib/jsoup-1.8.3.jar:../lib/xsoup-0.1.0.jar: WikiPagePopularity    
       (The Xms(minimum) and Xmx(maximum) set the heasp space to 300MB and 3GB respectively)