import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

public class Crawler {

	private HashMap<String, Long> linksForPopularityCount = new HashMap<String, Long>();
	private long count = 0;
	final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
	private Connection connection = null;
	Document htmlDocument = null;
	
	
	public void crawl(String url) throws IOException {
		connection = Jsoup.connect(url).userAgent(USER_AGENT);
	    htmlDocument = null;
		try {
			htmlDocument = connection.get();
			
		} catch (HttpStatusException e) {
			htmlDocument = null;
		}

		catch (IOException e) {
			htmlDocument = null;
		}
		if (htmlDocument != null) {
			Element div = htmlDocument.select("div.mw-content-ltr").first();
			Elements links = div.getElementsByAttribute("title");
			Element whatLinksHere = null;
			Element link = null;
			for (int i = 0; i < links.size(); i++) {
				System.out.println(i+1 + " of " + links.size());
				if (!links.get(i).absUrl("href").contains("Category") && !links.get(i).absUrl("href").contains("Wikipedia:")) {
					try {
						System.out.println(links.get(i).absUrl("href"));
						if(this.linksForPopularityCount.get(links.get(i).absUrl("href")) != null){
							continue;
						}
						connection = Jsoup.connect(links.get(i)
								.absUrl("href")).userAgent(USER_AGENT);
						htmlDocument = connection.get();
						whatLinksHere = htmlDocument.getElementsByAttributeValue("id","t-whatlinkshere").first();
						if (whatLinksHere != null) {
							link = whatLinksHere.child(0);
							this.getLinkCount(link.absUrl("href"));
							System.out.println("Totalcount:  " + count);
							linksForPopularityCount.put(links.get(i).absUrl("href"),
									this.count);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
	}

	public void getLinkCount(String url) throws IOException {
	  connection = Jsoup.connect(url).userAgent(USER_AGENT);
		htmlDocument = connection.get();
		String link = "https://en.wikipedia.org"
				+ Xsoup.compile("//*[@id=\"mw-content-text\"]/a[6]/@href")
						.evaluate(htmlDocument).get();
		 connection = Jsoup.connect(link).userAgent(USER_AGENT); 
		try {
			this.count = 0;
			this.getNextPage(link, 1);
			System.out.println("Ended next page function, control in getLinkCount function");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getNextPage(String url, long pageNo) throws IOException {
		connection = Jsoup.connect(url).timeout(0).userAgent(USER_AGENT);
		htmlDocument = null;
		htmlDocument = connection.get();
		Element ul = htmlDocument.getElementById("mw-whatlinkshere-list");
		Elements li = ul.getElementsByTag("li");
		count = count + li.size();
		String next = null;
		System.out.println("pageNo is: " + pageNo);

		if (pageNo == 1) {
			next = "https://en.wikipedia.org"
					+ Xsoup.compile("//*[@id=\"mw-content-text\"]/a[1]/@href")
							.evaluate(htmlDocument).get();
		} else {
			if (Xsoup.compile("//*[@id=\"mw-content-text\"]/a[2]/text()")
					.evaluate(htmlDocument).get().contains("next")) {
				next = "https://en.wikipedia.org"
						+ Xsoup.compile(
								"//*[@id=\"mw-content-text\"]/a[2]/@href")
								.evaluate(htmlDocument).get();
			}
			
		}
		if (next != null) {
			System.out.println(next);
			this.getNextPage(next, pageNo + 1);
		}

	}

	public void printPopularLink() {
		long maxValueInMap = (Collections.max(linksForPopularityCount.values()));
		for (Entry<String, Long> entry : linksForPopularityCount.entrySet()) {
		if (entry.getValue() == maxValueInMap) {
			System.out.println("--------------------------------------------------------------------------------------");
			System.out.println("Popular link is: " + entry.getKey() + " With :"
					+ entry.getValue() + " counts");
			System.out.println("--------------------------------------------------------------------------------------");
			}
		}

	}

}
