import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class WikiPagePopularity {

	public static void main(String[] args) throws IOException {

		WikiPagePopularity wikiPagePopularity = new WikiPagePopularity();
		
		Crawler crawler = new Crawler();
		String inputUrl = wikiPagePopularity.getUrl();
		crawler.crawl(inputUrl);
		crawler.printPopularLink();
	}
	
	
	private String getUrl(){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Wikipedia page url:");
		try{
		String inputUrl = reader.readLine();
		if(!inputUrl.contains("wikipedia")){
			System.out.println("Given input is not a wikipedia link");
			getUrl();
		}
		URL url = new URL(inputUrl);
		URLConnection conn = url.openConnection();
	    conn.connect();
	   
	    
	   return inputUrl;
	}catch (MalformedURLException e) {
	   System.out.println("Invalid Url. Please enter a valid Url");
	   getUrl();
	} 
	
	catch (IOException e) {
		System.out.println("Invalid Url. Please enter a valid Url");
		getUrl();
	}
		return null;
	}

	
}
